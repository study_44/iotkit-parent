package cc.iotkit.message.notify;

/**
 * author: 石恒
 * date: 2023-05-08 15:21
 * description:
 **/
public enum EventType {
    MQ, Message
}
